<?php
namespace CHDuBar\PruebaTpagaEcommerce\Utility;

class TpagaConnectionUtility {
    
    
	public static function initializeCUrl($endpoint, &$credenciales, $json_array, $isPost = true){
        //create a new cURL resource
        $curl = curl_init($endpoint);
        $headers = array(
            'Authorization: Basic '. base64_encode($credenciales['user'].':'.$credenciales['password']),
            'Cache-Control: no-cache',
            'Content-Type: application/json'
        );
        
        //Tell cURL that we want to send a POST request.
        if($isPost){
            curl_setopt($curl, CURLOPT_POST, 1);
        } 
        //set the content type to application/json
        curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, 1);        
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //return response instead of outputting
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if(!is_null($json_array)){//attach encoded JSON string to the POST fields     
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($json_array));
         }
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        return $curl;
    }

    public static function payment_request(&$credenciales, &$json_array){               
        $curl = self::initializeCUrl('https://stag.wallet.tpaga.co/merchants/api/v1/payment_requests/create', $credenciales, $json_array);        
 
        //execute the POST request
        $result = curl_exec($curl);        
        $resultados_decodificados = json_decode($result);
        $final_result = array();
        if($resultados_decodificados->error_code){
            $final_result = self::assingError($resultados_decodificados);
        } else {
            $final_result['tpagaUrl'] = $resultados_decodificados->tpaga_payment_url;
            $final_result['token'] = $resultados_decodificados->token;
        }
        return $final_result;
    }

    public static function validate_sale(&$credenciales, &$json_array){    
        $curl = self::initializeCUrl('https://stag.wallet.tpaga.co/merchants/api/v1/payment_requests/'.$json_array['payment_request_token'].'/info', $credenciales, null, false);                
        return self::evaluateStatusParameter($curl); //execute the GET request
    }

    public static function delivery_product(&$credenciales, &$json_array){               
        $curl = self::initializeCUrl('https://stag.wallet.tpaga.co/merchants/api/v1/payment_requests/confirm_delivery', $credenciales, $json_array);        
        return self::evaluateStatusParameter($curl); //execute the POST request
    }

    public static function revert_sale(&$credenciales, &$json_array){               
        $curl = self::initializeCUrl('https://stag.wallet.tpaga.co/merchants/api/v1/payment_requests/refund', $credenciales, $json_array);        
        return self::evaluateStatusParameter($curl); //execute the POST request
    }

    public static function evaluateStatusParameter(&$curl){
        $result = curl_exec($curl);               
        $resultados_decodificados = json_decode($result);        
        $final_result = array();
        if($resultados_decodificados->error_code){
            $final_result = self::assingError($resultados_decodificados);
        } else {
            $final_result['status'] = $resultados_decodificados->status;
        }
        return $final_result;
    }

    public static function assingError(&$response){
        $final_result['error'] = true;
        $final_result['message'] = $response->error_message;
        return $final_result;
    }
}