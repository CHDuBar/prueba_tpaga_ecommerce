<?php
namespace CHDuBar\PruebaTpagaEcommerce\Controller;

use CHDuBar\PruebaTpagaEcommerce\Domain\Model\Customer;
use CHDuBar\PruebaTpagaEcommerce\Domain\Model\Sale;
use CHDuBar\PruebaTpagaEcommerce\Domain\Model\IdempotencyKey;
use CHDuBar\PruebaTpagaEcommerce\Utility\TpagaConnectionUtility;
/***
 *
 * This file is part of the "Ecommerce Videojuegos Tpaga" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 César Hansy Dueñas Barragán <cesart234@gmail.com>
 *
 ***/

/**
 * SaleController
 */
class SaleController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * saleRepository
     *
     * @var \CHDuBar\PruebaTpagaEcommerce\Domain\Repository\SaleRepository
     * @inject
     */
    protected $saleRepository = null;

    /**
     * productRepository
     *
     * @var \CHDuBar\PruebaTpagaEcommerce\Domain\Repository\ProductRepository
     * @inject
     */
    protected $productRepository = null;

    /**
     * customerRepository
     *
     * @var \CHDuBar\PruebaTpagaEcommerce\Domain\Repository\CustomerRepository
     * @inject
     */
    protected $customerRepository = null;

    /**
     * frontendUserGroupRepository
     *
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserGroupRepository
     * @inject
     */
    protected $frontendUserGroupRepository = null;

    /**
     * idempotencyKeyRepository
     *
     * @var \CHDuBar\PruebaTpagaEcommerce\Domain\Repository\IdempotencyKeyRepository
     * @inject
     */
    protected $idempotencyKeyRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {       
        $currentUser = $GLOBALS['TSFE']->fe_user->user;
        if ($currentUser) {
            $user = $this->customerRepository->findByUid($currentUser['uid']);
            $sales = $this->saleRepository->findByBuyer($user);
            $this->view->assign('frontendUser', $user);
        } else {
            $sales = $this->saleRepository->findAll();            
        }
        $this->view->assign('sales', $sales);
    }

    /**
     * action show
     *
     * @param \CHDuBar\PruebaTpagaEcommerce\Domain\Model\Sale $sale
     * @return void
     */
    public function showAction(\CHDuBar\PruebaTpagaEcommerce\Domain\Model\Sale $sale)
    {
        $this->view->assign('sale', $sale);
    }

    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {
        $arguments = $this->request->getArguments();
        $product = $this->productRepository->findByUid($arguments['product']);
        if (!is_null($product)) {
            $this->view->assign('product', $product);
        }
        $amount = $arguments['amount'] > 0 ? $arguments['amount'] : 1;
        $this->view->assign('amount', $amount);
        $this->view->assign('totalPay', $amount * $product->getPrice());
    }

    /**
     * action create
     *
     * @return void
     */
    public function createAction()
    {
        $arguments = $this->request->getArguments();
        #Creation of customer
        $customer_exist = $this->customerRepository->findByUsername($arguments['customer']['email'])->getFirst();
        if ($customer_exist) {
            $customer = $customer_exist;
        } else {
            $customer = $this->createCustomer($arguments['customer']);
        }
        #Get product
        $product = $this->productRepository->findByUid($arguments['product']);
        #Cration of sale
        $isTokenExist = true;
        //Premisa existe token
        do {    $idempotencyToken = new IdempotencyKey();
            $token_repeat = $this->idempotencyKeyRepository->findByToken($idempotencyToken->getToken())->getFirst();
            if (is_null($token_repeat)) {
                $isTokenExist = false;
            }
        } while ($isTokenExist);
        $sale = new Sale($arguments, $customer, $product, $idempotencyToken);
        #Creation of Billing
        $orderId = random_int(100000, 999999);
        /** Tpaga integration */
        #Create payment request
        $cObj = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\ContentObject\\ContentObjectRenderer');
        $conf = [
            'parameter' => 13,
            'useCashHash' => false,
            'returnLast' => 'url',
            'forceAbsoluteUrl' => true
        ];
        $link_purchase = $cObj->typolink_URL($conf);
        $conf['parameter'] = 14;
        $link_voucher = $cObj->typolink_URL($conf);
        #Create purchased items array
        $purchased_items = [];
        $purchased_items[] = [
            'name' => $product->getName(),
            'value' => $product->getPrice(),
            'amount' => $sale->getAmount()
        ];
        #Expires date
        $expires_date = new \DateTime('NOW');
        $startTime = date('Y-m-d H:i:s');
        $convertedTime = date('Y-m-d H:i:s', strtotime('+1 day', strtotime($startTime)));
        //$expires_date->format('Y-m-d H:i:s');
        //$expires_date->format(\DateTime::ATOM);
 $callback = "{$link_purchase}".'&identoken='.$idempotencyToken->getToken() ;
        $json_array = [
            'cost' => $arguments['totalPay'],
            'purchase_details_url' => "{$callback}",
            'voucher_url' => "{$link_voucher}",
            'idempotency_token' => $idempotencyToken->getToken(),
            'order_id' => $sale->getIdentifier(),
            'terminal_id' => 'caja_' . $sale->getCheckout(),
            'purchase_description' => 'Compra en la ' . $checkout . ' de ' . $arguments['amount'] . ' juego(s) del titulo ' . $product->getName() . ' por un valor total de ' . $arguments['totalPay'],
            'purchase_items' => $purchased_items,
            'user_ip_address' => $sale->getUserIpAddress(),
            'expires_at' => date('c', strtotime($convertedTime))
        ];
        $credentials = $this->credencialsRequest();
        $payment_request_result = TpagaConnectionUtility::payment_request($credentials, $json_array);
        if ($payment_request_result['error']) {
            $this->addFlashMessage('Se ha generado un error al enviar la información a la pasarela de pago. ' . $payment_request_result['message'], '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
            $this->redirect('list', 'Product');
        } else {
            $sale->setLinkPay(json_encode($payment_request_result));
            $this->saleRepository->add($sale);
            $this->makePersistence();
            #Redirect link
            $this->redirectToUri($payment_request_result['tpagaUrl']);
        }
    }

    /**
     * @param $customer_array
     */
    public function createCustomer(&$customer_array)
    {
        if (\TYPO3\CMS\Saltedpasswords\Utility\SaltedPasswordsUtility::isUsageEnabled('FE')) {
            $objSalt = \TYPO3\CMS\Saltedpasswords\Salt\SaltFactory::getSaltingInstance(NULL);
            if (is_object($objSalt)) {
                $saltedPassword = $objSalt->getHashedPassword($customer_array['password']);
            }
        }
        $customer_array['group'] = $this->frontendUserGroupRepository->findByUid(2);
        $customer_array['password'] = $saltedPassword;
        $customer = new Customer($customer_array);
        $this->customerRepository->add($customer);
        return $customer;
    }

    /**
     * action registerPurchase
     *
     * @param \CHDuBar\PruebaTpagaEcommerce\Domain\Model\Sale $sale
     * @return void
     */
    public function registerPurchaseAction(\CHDuBar\PruebaTpagaEcommerce\Domain\Model\Sale $sale = null)
    {
        $credentials = $this->credencialsRequest();
        if (is_null($sale)) { #Callback origin
            $currentUrl = $this->uriBuilder->getRequest()->getRequestUri();
            $urlPartsArr = explode('&', $currentUrl);
            $requestGETParams = array();
            foreach ($urlPartsArr as $element) {
                $left = explode('=', $element)[0];
                $right = isset(explode('=', $element)[1]) ? explode('=', $element)[1] : '';
                $requestGETParams[$left] = $right;
            }
            $idempotencyToken = $this->idempotencyKeyRepository->findByToken($requestGETParams['identoken'])->getFirst();
            if($idempotencyToken){
                $sale = $this->saleRepository->findByIdempotencyKey($idempotencyToken)->getFirst();
                $json_array = ['payment_request_token' => json_decode($sale->getLinkPay())->token];
                $status_result = TpagaConnectionUtility::validate_sale($credentials, $json_array);
                if ($status_result['error']) {
                    $this->addFlashMessage('Se ha generado un error al hacer la validación del pago. ' . $status_result['message'], '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
                } else {
                    $sale->validateStatus($status_result['status']);
                    if (in_array($status_result['status'], ['paid', 'delivered'])) {
                        $sale->getPurchasedProduct()->decreaseStock($sale->getAmount());
                    }
                    $this->saleRepository->update($sale);
                    $this->makePersistence();
                    $this->addFlashMessage('Su pago se ha registrado, procedemos con el envío del producto.', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
                }
            } else {
                $this->addFlashMessage('El token no es válido.', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
            }
            $this->redirect('list', 'Product', null, null, 8);

        } else { #Backend administration
            $json_array = ['payment_request_token' => json_decode($sale->getLinkPay())->token];
            $status_result = TpagaConnectionUtility::validate_sale($credentials, $json_array);
            if ($status_result['error']) {
                $this->addFlashMessage('Se ha generado un error al hacer la validación del estado. ' . $status_result['message'], '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
            } else {
                $sale->validateStatus($status_result['status']);
                if (in_array($status_result['status'], ['expired', 'reverted', 'failed'])) {
                    $sale->getPurchasedProduct()->increaseStock($sale->getAmount());
                }
                $this->saleRepository->update($sale);
                $this->makePersistence();
                $this->addFlashMessage('Se verificó y actualizó el estado de la venta', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
            }
            $this->redirect('list');
        }
    }

    /**
     * action delivery
     *
     * @param \CHDuBar\PruebaTpagaEcommerce\Domain\Model\Sale $sale
     * @return void
     */
    public function deliveryAction($sale)
    {
        $credentials = $this->credencialsRequest();
        $json_array = ['payment_request_token' => json_decode($sale->getLinkPay())->token];
        $delivery_result = TpagaConnectionUtility::delivery_product($credentials, $json_array);
        if ($delivery_result['error']) {
            $this->addFlashMessage('Se ha generado un error al intentar registrar la entreda del producto. ' . $delivery_result['message'], '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
        } else {
            $sale->validateStatus($delivery_result['status']);
            $this->saleRepository->update($sale);
            $this->makePersistence();
            $this->addFlashMessage('Se registró la entrega del producto.', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
        }
        $this->redirect('list');
    }

    /**
     * action revert
     *
     * @param \CHDuBar\PruebaTpagaEcommerce\Domain\Model\Sale $sale
     * @return void
     */
    public function revertAction($sale)
    {
        $credentials = $this->credencialsRequest();
        $json_array = ['payment_request_token' => json_decode($sale->getLinkPay())->token];
        $revert_sale_result = TpagaConnectionUtility::revert_sale($credentials, $json_array);
        if ($revert_sale_result['error']) {
            $this->addFlashMessage('Se ha generado un error al intentar devolver un pago. ' . $revert_sale_result['message'], '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
        } else {
            $sale->validateStatus($delivery_result['status']);
            $sale->getPurchasedProduct()->increaseStock($sale->getAmount());
            $this->saleRepository->update($sale);
            $this->makePersistence();
            $this->addFlashMessage('Se registró la devolución de la venta.', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
        }
        $this->redirect('list');
    }

    public function credencialsRequest()
    {
        $credentials = ['user' => null, 'password' => null];
        $extConf = @unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['prueba_tpaga_ecommerce']);
        $credentials['user'] = $extConf['tpagaconnection.']['user'];
        $credentials['password'] = $extConf['tpagaconnection.']['password'];
        return $credentials;
    }

    public function makePersistence()
    {
        $persistenceManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager');
        $persistenceManager->persistAll();
    }

}
