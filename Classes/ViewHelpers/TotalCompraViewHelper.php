<?php
namespace CHDuBar\PruebaTpagaEcommerce\ViewHelpers;

class TotalCompraViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper{
	
	/**
	 * 
     * @param int $value
     * @param int $count
	 * @return statusLabel
	 */
	public function render($value, $count) {	        
		return $value*$count;
	}
}