<?php
namespace CHDuBar\PruebaTpagaEcommerce\ViewHelpers;

class IdentifyStatusViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper{
	
	/**
	 * 
	 * @param string $statusId
	 * @return statusLabel
	 */
	public function render($statusId) {	
        switch($statusId){
            case 1:
                $statusLabel = 'Pagado';
            break;
            case 2:
                $statusLabel = 'Fallo';
                break;
            case 3:
                $statusLabel = 'Entregado';
                break;
            case 4:
                $statusLabel = 'Devuelto';
                break;
            default:
                $statusLabel = 'Creado';
        }
		return $statusLabel;
	}
}