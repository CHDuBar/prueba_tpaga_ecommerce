<?php
$GLOBALS['TCA']['tx_pruebatpagaecommerce_domain_model_sale']['columns']['status']['config']['items'] = [
    ['LLL:EXT:prueba_tpaga_ecommerce/Resources/Private/Language/locallang_db.xlf:tx_pruebatpagaecommerce_domain_model_sale.creado', 0],
    ['LLL:EXT:prueba_tpaga_ecommerce/Resources/Private/Language/locallang_db.xlf:tx_pruebatpagaecommerce_domain_model_sale.pagado', 1],
    ['LLL:EXT:prueba_tpaga_ecommerce/Resources/Private/Language/locallang_db.xlf:tx_pruebatpagaecommerce_domain_model_sale.fallo', 2],
    ['LLL:EXT:prueba_tpaga_ecommerce/Resources/Private/Language/locallang_db.xlf:tx_pruebatpagaecommerce_domain_model_sale.entregado', 3],
    ['LLL:EXT:prueba_tpaga_ecommerce/Resources/Private/Language/locallang_db.xlf:tx_pruebatpagaecommerce_domain_model_sale.devuelto', 4],
    ['LLL:EXT:prueba_tpaga_ecommerce/Resources/Private/Language/locallang_db.xlf:tx_pruebatpagaecommerce_domain_model_sale.expiro', 5],
];